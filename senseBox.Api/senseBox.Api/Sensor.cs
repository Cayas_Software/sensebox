﻿using System;
using Newtonsoft.Json;

namespace SenseBox.Api
{
    public class Sensor
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        public LastMeasurement LastMeasurement { get; set; }
        public string SensorType { get; set; }
        public string Title { get; set; }
        public string Unit { get; set; }
        public string Icon { get; set; }
    }
}
