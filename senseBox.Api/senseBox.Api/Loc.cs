﻿using System;

namespace SenseBox.Api
{
    public class Loc
    {
        public Geometry Geometry { get; set; }
        public string Type { get; set; }
    }
}
