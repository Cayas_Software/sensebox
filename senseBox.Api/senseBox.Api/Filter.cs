﻿using System;

namespace SenseBox.Api
{
    public class Filter
    {
        DateTime Date { get; set; }
        string Format { get; set; }
    }
}
