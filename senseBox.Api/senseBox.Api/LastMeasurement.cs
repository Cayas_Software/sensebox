﻿using System;

namespace SenseBox.Api
{
    public class LastMeasurement
    {
        public string Value { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
