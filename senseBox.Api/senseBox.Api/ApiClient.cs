﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SenseBox.Api
{
    public class ApiClient
    {
        HttpClient _httpClient;

        public ApiClient()
        {
            var httpClientHandler = new HttpClientHandler();

            if (httpClientHandler.SupportsAutomaticDecompression)
                httpClientHandler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            Init(httpClientHandler);
        }

        public ApiClient(HttpClientHandler handler)
        {
            Init(handler);
        }

        void Init(HttpClientHandler handler)
        {
            _httpClient = new HttpClient(handler) { BaseAddress = new Uri("https://api.opensensemap.org/") };
        }

        public async Task<List<Box>> GetBoxes(Filter filter)
        {
            var response = await QueryGetResult("boxes/");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Box>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Box> GetBox(string boxId)
        {
            var response = await QueryGetResult("boxes/" + boxId);
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<Box>(await response.Content.ReadAsStringAsync());
        }

        public async Task<BoxMeasurements> GetLastMeasurements(string boxId)
        {
            var response = await QueryGetResult($"boxes/{boxId}/sensors");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<BoxMeasurements>(await response.Content.ReadAsStringAsync());
        }

        Task<HttpResponseMessage> QueryGetResult(string url)
        {
            return _httpClient.GetAsync(url);
        }
    }
}

