﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SenseBox.Api
{
    public class Box
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        public string BoxType { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Exposure { get; set; }
        public string GroupTag { get; set; }
        public string Image { get; set; }
        public List<Loc> Loc { get; set; }
        public string name { get; set; }
        public List<Sensor> Sensors { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    public class BoxMeasurements
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        public List<Sensor> Sensors { get; set; }
    }
}
