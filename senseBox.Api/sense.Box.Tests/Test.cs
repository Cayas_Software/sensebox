﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SenseBox.Tests
{
    [TestFixture]
    public class Test
    {
        Api.ApiClient _client;

        [OneTimeSetUp]
        public void SetUp()
        {
            _client = new Api.ApiClient();
        }

        [Test]
        public async Task GetBoxes()
        {
            var boxes = await _client.GetBoxes(null);

            Assert.IsNotNull(boxes);
        }

        [Test]
        public async Task GetBox()
        {
            var box = await _client.GetBox("59607bb394f0520011ad22fa");

            Assert.IsNotNull(box);
        }

        [Test]
        public async Task GetBoxLatestMeasurements()
        {
            var latestMeasurements = await _client.GetLastMeasurements("59607bb394f0520011ad22fa");

            Assert.IsNotNull(latestMeasurements);
        }
    }
}
